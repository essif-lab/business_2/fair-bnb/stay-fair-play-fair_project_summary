## Stay Fair Play Fair

![diagram](https://pavilion-assets.nyc3.digitaloceanspaces.com/stayfairplayfair/stayfairplayfair/1630604210627.jpg)

### What is it?

The Coop Passport provides portable membership between cooperatives.  It is based on mutual trust and a 'commons' of membership and resource exchanges between three founding co-operatives: FairBnB, Pavilion and Resonate.  They have founded a consortium to launch a use case centered on community-powered independent music micro-touring and tourism - "Stay Fair, Play Fair" - later expanding it to a wider cooperative platform ecosystem.

### How does it work?

A co-op industry body and/or designated Co-operatives issues verifiable credentials using a standardised Know Your Cooperator process. Members hold and present these credentials to new co-ops within the network, who them verify them before granting access. Partner co-operatives mutualise the KYC process costs through a separate accounting process.

### What are the Benefits?

The Cooperative Password will save KYC costs, reduce sign-up friction, boost membership and enable privacy-respecting cross-membership deals and ‘affinity’ offers. Co-operatives could reduce risk where membership checks are mutually recognised and trusted.

### Is this relevant to SSI?

The Cooperative Passport is a verifiable credential. Members will control their own credential and find it easier to join other co-operatives without repeated verification of original identity proofs.  They will only need to share what is necessary to join. Co-operatives will work with other co-operatives, governed in line with ICA principle 6, to mutualise the cost of KYC, respect the privacy of their members and reduce the risk of unnecessary aggregation, correlation and duplication of members' data.

### What is the SSI Challenge?

There are many examples of SSI technologies, but few of them have been tried or adopted at any scale.  Usability and adoption is an obstacle for many SSI startups.  Our consortium is offering clear benefits to our established communities who share common values.  We can work with our communities to refine and get the offer right.

### How will we explain and test it?

Simple metaphors can help:  Passports and Badges are familiar.  It helps to be able to show and celebrate what you bring to your community or take to other communities.   Wear your 'badge' on your shirt when appropriate ….and keep it in your pocket / wallet when it isn’t.  We can do this digitally for our memberships, across our communities.

### Where will we start?

Fairbnb and Resonate will start by trialling a co-operative habitat for tourists, local independent music and musicians with a 'commons' of membership and resource exchange. "Stay Fair, Play Fair". Pavilion will also implement a verification process for "Community" membership in Pavilion with associated discounts on software development services.

### Why is this new? Why will it succeed?

Independent, sustainable music micro-touring and tourism can compete with increasingly expensive global mega-touring or festival operations which tend to bring less spend, and have less cultural connection with the local scene.  It is a growing market.

### What is the business model?

Artists gain more as a proportion of revenue and guests have a more authentic experience from participation in community-rooted independent music. Fairbnb will reference and link to Resonate and feature Local Artists, acting as a marketing/sales agent, helping to recruit local artists and curators, establish listener accounts and drive earnings for artists.  The Resonate Player and Community Forum / Artists' Circles feature invite links to Fairbnbcoop lodging offers in communities associated with Artists (home towns or touring locations).  Artists will become both guests and potential new hosts, mainly for local experiences. 
